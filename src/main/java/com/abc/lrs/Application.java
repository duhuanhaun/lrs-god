package com.abc.lrs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


/**
 * @author duhuanhuan
 * @version V1.0
 * @Title: Application
 * @Package com.abc.lrs
 * @Description:
 * @date 2017/10/11 15:56
 */
@SpringBootApplication
@ComponentScan("com.abc.lrs")
public class Application {
	public static final void main(String args[]){
		SpringApplication.run(Application.class,args);
	}
}
