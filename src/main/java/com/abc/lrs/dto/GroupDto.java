package com.abc.lrs.dto;

import java.util.List;

/**
 * @author duhuanhuan
 * @version V1.0
 * @Title: GroupDto
 * @Package com.abc.lrs.dto
 * @Description:
 * @date 2017/10/11 16:25
 */
public class GroupDto {
	private Long groupId;
	private String groupName;
	private String groupPopulation;
	private String groupPwd;
	private List<RoleInfoDto> roleInfos;

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupPopulation() {
		return groupPopulation;
	}

	public void setGroupPopulation(String groupPopulation) {
		this.groupPopulation = groupPopulation;
	}

	public String getGroupPwd() {
		return groupPwd;
	}

	public void setGroupPwd(String groupPwd) {
		this.groupPwd = groupPwd;
	}
}
