package com.abc.lrs.dto;

/**
 * @author duhuanhuan
 * @version V1.0
 * @Title: RoleInfoDto
 * @Package com.abc.lrs.dto
 * @Description:
 * @date 2017/10/11 17:01
 */
public class RoleInfoDto {

	private String role;

	private int roleNumbers;

	private int roleStatus;
}
