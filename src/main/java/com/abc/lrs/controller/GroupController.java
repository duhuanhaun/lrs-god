package com.abc.lrs.controller;

import com.abc.lrs.dto.GroupDto;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author duhuanhuan
 * @version V1.0
 * @Title: GroupController
 * @Package com.abc.lrs.controller
 * @Description:
 * @date 2017/10/11 16:06
 */
@Controller
public class GroupController {
	private static final Logger logger = LoggerFactory.getLogger(GroupController.class);
	@RequestMapping(value = "/create/group",method = RequestMethod.POST)
	@ResponseBody
	public Object createGroup(@RequestBody GroupDto groupDto){
		Gson gson = new Gson();
		logger.info("request---->"+gson.toJson(groupDto));
	   try{
		   return groupDto;
	   }catch (Exception e){
		   logger.error("系统异常",e);
		   return "系统异常";
	   }

	}
}
